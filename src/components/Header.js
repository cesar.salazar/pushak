import React from 'react';
import {useTranslation} from "react-i18next";
import { Link, animateScroll as scroll } from "react-scroll";
export default function Header(){
        const [t, i18n] =useTranslation("global");
    return (
        <div>
            <header id="header" class="fixed-top d-flex align-items-center  header-transparent ">
    <div class="container d-flex align-items-center justify-content-between">

      <div class="logo">
        <h1 class="text-light"><a href="/">Pushak <span>Technologies</span></a></h1>
        
        {/* <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>--> */}
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href=""><Link to="hero">{t("header.home")}</Link></a></li>
          <li><a href="about"><Link to="about">{t("header.about-us")}</Link></a></li>
          <li><a href="services"><Link to="services">{t("header.solutions")}</Link></a></li>
          <li><a href="portfolio"><Link to="portfolio">{t("header.portfolio")}</Link></a></li>
          <li><a href="price"><Link to="pricing">{t("header.price")}</Link></a></li>
          <li><a href="team"><Link to="team">{t("header.team")}</Link></a></li>
          <li><a href="contact"><Link to="contact">{t("header.contact")}</Link></a></li>
          <li  class="drop-down"><a href="">{t("header.language")}</a>
            <ul>
              <li class="lenguajes"><a onClick={()=>i18n.changeLanguage("es") && localStorage.setItem("lg", 'es')}><img class="flag" src="./assets/img/es_flag.png"/></a></li>
              <li class="lenguajes"><a class="en" onClick={()=>i18n.changeLanguage("en") && localStorage.setItem("lg", 'en')}><img class="flag" src="./assets/img/en_flag.png"/></a></li>
            </ul>
          </li>
        </ul>
      </nav>

    </div>
  </header>
  
        </div>
    );
}